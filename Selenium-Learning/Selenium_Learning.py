import configparser
import time
import random
import string
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

def search_page(driver, query, element_type):
    if element_type is 'id':
        return driver.find_element_by_id(query)
    elif element_type is 'name':
        return driver.find_element_by_name(query)
    elif element_type is 'xpath':
        return driver.find_element_by_xpath(query)

def click_element(driver, query, element_type):
    element = search_page(driver, query, element_type)
    element.click()

def mouse_over(driver, query, element_type):
    element = search_page(driver, query, element_type)

    hover = ActionChains(driver).move_to_element(element)
    hover.perform()


def new_tab(driver, link):
    if 'http://' in link:
        pass
    else:
        link = 'http://' + link

    driver.execute_script('window.open("");')
    driver.switch_to_window(driver.window_handles[-1])
    driver.get(link)
    return driver

def test_BlogLogin(driver, config):
    driver.maximize_window()
    driver.get( config['blog_login']['link'] )
    time.sleep(1)
    click_element(driver, '(//li)[3]/a', 'xpath')
    time.sleep(2)
    user_name = search_page(driver, 'user_login', 'id')
    password = search_page(driver, 'user_pass', 'id')

    user_name.send_keys( config['blog_login']['user'])
    password.send_keys( config['blog_login']['password'])

    click_element(driver, 'wp-submit', 'id')
    
    if 'wp-admin' in driver.current_url:
        print(test_BlogLogin.__name__ + ' Passed')
    else:
        print(test_BlogLogin.__name__ + ' Failed')
    
def test_BlogLogout(driver):
    if 'wp-admin' in driver.current_url:
        mouse_over(driver, 'wp-admin-bar-my-account', 'id')
        time.sleep(2)
        click_element(driver, 'wp-admin-bar-logout', 'id')
        time.sleep(1)

        if 'loggedout=true' in driver.current_url:
            print(test_BlogLogout.__name__ + ' Passed')
        else:
            print(test_BlogLogout.__name__ + ' Failed')

def test_BlogPost(driver, config):
    click_element(driver, 'menu-posts', 'id')
    time.sleep(2)
    click_element(driver, '//*[@id="wpbody-content"]/div[3]/a', 'xpath')
    time.sleep(5)
    title = search_page(driver, 'title', 'id')
    click_element(driver, 'content-html', 'id')
    body = search_page(driver, 'content', 'name')

    title.send_keys(config['blog_post']['title'])
    body.send_keys(config['blog_post']['body'])

    click_element(driver, 'publish', 'id')

def test_DeletePost(driver):
    click_element(driver, 'delete-action', 'id')

def test_CommentPost(driver, config):
    driver.get( config['post_comment']['link'] )
    text_box = search_page(driver, 'comment', 'id')

    comment = ''.join( random.choice(string.ascii_lowercase) for _ in range(20))

    text_box.send_keys( comment )
    click_element(driver, 'submit', 'id')

def test_Register(driver, config):
    # goto webpage that provides email (probably: 10minutemail.net)
    driver.get( config['test_register']['email_link'] )

    # search email box on page
    email = search_page(driver, 'fe_text', 'id')

    # get value attribute of the email box (this contains the email address)
    email = email.get_attribute('value')

    # goto register webpage (see config for full link)
    driver.get( config['test_register']['register_link'] )
    
    # generate random 10 character username
    username = ''.join( random.choice(string.ascii_lowercase) for _ in range(10))
    
    # find username box on register page
    user_login = search_page(driver, 'user_login', 'id')

    # wait 2 seconds
    time.sleep(2)

    # input the random generated username
    user_login.send_keys(username)

    # find email box on register page
    user_email = search_page(driver, 'user_email', 'id')

    # input the 10minutemail email address in the box
    user_email.send_keys(email)

    # click Register button
    click_element(driver, 'wp-submit', 'id')

    # check if registration was successful
    if 'checkemail=registered' in driver.current_url:
        # goto webpage that provides email (probably: 10minutemail.net)
        driver.get( config['test_register']['email_link'] )

        # click the most recent email in the list
        click_element(driver, '//*[@id="maillist"]/tbody/tr[2]/td[2]/a', 'xpath')

        # click the register confirmation link
        click_element(driver, '//*[@id="tab3"]/p/a[1]', 'xpath')

        # find password box
        password = search_page(driver, 'pass1-text', 'id')

        # get password from box
        password = password.get_attribute('value')
        
        # click the reset password button (this actually sets the password)
        click_element(driver, 'wp-submit', 'id')

        # check if password was reset
        if 'action=resetpass' in driver.current_url:
            #click the login button
            click_element(driver, '//*[@id="login"]/p[1]/a', 'xpath')

            print('Succes')


    else:
        # test failed - registration failed
        print('Registration failed because stuff')



def main():
    driver = webdriver.Chrome('E:\\Work\\PythonMaterials\\Selenium\\chromedriver.exe')
    config = configparser.ConfigParser()
    config.read('config.ini')

    #test_BlogLogin(driver, config)
    #time.sleep(5)
    #test_BlogPost(driver, config)
    #time.sleep(5)
    #test_DeletePost(driver)
    #time.sleep(5)
    #test_CommentPost(driver,config)
    #time.sleep(5)
    #test_BlogLogout(driver)
    test_Register(driver, config)
    

main()